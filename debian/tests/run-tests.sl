variable f;

() = evalfile ("./libdir/libdir.sl");

if (getenv ("USE_LOCAL_LIB") != NULL)  {
    foreach f (listdir (".")) 
        if (file_status (f) == 2)
            append_libdir (f, 1);
    % This is necessary for test-css1.sl to succeed
    add_mode_for_extension ("css1", "css");
} else 
    append_libdir ("/usr/share/jed/jed-extra/extra/", 1);

require ("bufutils");
require ("ch_table");
require ("csvutils");
require ("css1");
require ("datutils");
require ("glob");
require ("rst-outline");
require ("structured_text");
require ("strutils");
require ("txtutils");
require ("unittest");

% Unit test files. Some of them are commented out, with the associated
% reason being given.
variable files = ["./tests/ch_table-test.sl",
		  "./tests/csvutils-test.sl",
% Only works with xjed
%		  "./tests/cuamouse-test.sl",
		  "./tests/cuamark-test.sl",
		  "./tests/datutils-test.sl",
% Yields strange error releated to fit_window()
%		  "./tests/ishell-test.sl",
		  "./tests/listing-test.sl",
% Invokes python instead of python3
%		  "./tests/pymode-test.sl",
		  "./tests/rst-outline-test.sl",
% Produces garbage on stack
% 		  "./tests/rst-test.sl",
		  "./tests/structured_text-test.sl",
		  "./tests/strutils-test.sl",
% Needs network connection
%		  "./tests/test-browse_url.sl",
% Tests need interactivity
%		  "./tests/test-calc.sl",
% Needs interactive testing in a Linux console
% 		  "./tests/test-console_keys.sl",
		  "./tests/test-css1.sl",
		  "./tests/test-dict-cli.sl",
% Needs network connection
		  "./tests/test-dict-curl.sl"];
% Contains path from upstream maintainer system
%		  "./tests/unittesttest.sl",
% Produces garbage on stack
%		  "./tests/utf8helper-autoconvert-test.sl",
%		  "./tests/utf8helper-test.sl"];

% Run the tests
foreach f (files)
   test_file (f);
