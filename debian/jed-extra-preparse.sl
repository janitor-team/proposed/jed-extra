% -*- slang -*-
% Prepare the S-Lang files installed by jed-extra for use with Jed.
% 
% * create ini.sl files with autoloads, completions and <INITIALIZATION>
%   blocks
% * byte_compile files

% Location of the jed-extra package's library dir
private variable lib_dir = "/usr/share/jed/jed-extra";
private variable drop_in_dir = path_concat(lib_dir, "drop-in");
private variable utils_dir = path_concat(lib_dir, "utils");
private variable extra_dir = path_concat(lib_dir, "extra");
private variable colors_dir = path_concat(lib_dir, "colors");

% add the jed-extra libraries to the jed library path
set_jed_library_path(strjoin([drop_in_dir, 
                              get_jed_library_path(),
                              lib_dir,
                              utils_dir], ","));

vmessage("jed library path: %s", get_jed_library_path());

% load utility functions
require("tm");
require("make_ini");

% Do not generate autoloads for the following files:
Make_ini_Exclusion_List = ["ini.sl",
                           "gdbmrecent.sl",
                           "ispell.sl",
                           "ispell_init.sl",
                           "ispell_common.sl",
                           "flyspell.sl",
                           "vispell.sl",
                           "look.sl",
                           % "hyperman.sl",
			   "calendar.sl",
                           "cal.sl"];

% Do not save backup copies of the generated files
No_Backups = 1;

% generate autoloads (but not completions) for all functions in Utils
Make_ini_Scope = 2;
Make_ini_Add_Completions = 0;
update_ini(utils_dir);

% preparse modes and generate ini.sl files with autoloads and completions for
% public functions
Make_ini_Scope = 1;
Make_ini_Add_Completions = 1;

update_ini(drop_in_dir);
update_ini(lib_dir);
update_ini(extra_dir);

% byte compile color files
byte_compile_libdir(colors_dir);
