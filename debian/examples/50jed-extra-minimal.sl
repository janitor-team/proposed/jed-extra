% 50jed-extra-minimal.sl: Non-invasive initialization of jed-extra package
%
% Copyright (c) 2005 Debian JED Group
%  Released under the terms of the GNU General Public License (v. 2 or later)
%
% The minimal version of the initalization file does a "non invasive"
% activation of the jed-extra modes.
% 
% Replace /etc/jed.d/50jed-extra.sl with this file, if the users of your site
% want to fine tune the level of jed-extr activation in their ~/.jed/jed.rc
% 
% All settings in this file can be reverted by normal users.

% Location of the jed-extra package's library dir
$1 = "/usr/share/jed/jed-extra/";

% if jed-extra is removed (not purged), this file (and possibly the
% jed-extra library dir) exist, but nothing else it expects --> abort
$2 = path_concat($1, "libdir.sl");
#if (file_status($2) != 1)
#stop
#endif

% Add (and initialize) library directories
% ----------------------------------------

() = evalfile($2);

% Utilities (required by the other modes)
%   Initialization adds autoloads for utility functions:
%    - slows down startup
%    + lets private modes use the util functions without need for autoloads
%    + lets Help>Apropos find util functions right from the start
% append_libdir($1 + "utils/", 1);    % append and initialize 
append_libdir($1 + "utils/", 0);  % append but do not initialize

% "Normal" modes
%   Initialization is low key (autolaods, some Menu entries)
% append_libdir($1, 1);   	    % append and initialize 
append_libdir($1, 0);             % append but do not initialize

% Drop-In Modes (recent, help, man, ...) need to be prepended
%   Comment if you want to keep the original behaviour
%   Initialization is low key (autolaods for additional functions)
% add_libdir($1 + "drop-in/", 1);     % append and initialize 
add_libdir($1 + "drop-in/", 0);   % append but do not initialize

% Experimental and exotic modes
%   The recommended use is to, copy or link interesting modes
%   to /usr/local/share/jed/lib/ or ~/.jed/lib and user make_ini() and
%   add_libdir() for activation (in ~/jed.rc).

% add some autoloads
autoload("push_defaults", "sl_utils");    % needed by ispell_init.sl, complete, occur, ...
autoload("string_nth_match", "strutils"); % needed by hyperman.sl
autoload("get_keystring", "strutils");    % needed by snake.sl
% alternatively evaluate the utils/ini.sl file
% () = evalfile("utils/ini.sl");          % autoloads for all utilit functions

